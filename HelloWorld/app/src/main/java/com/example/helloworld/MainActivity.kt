package com.example.helloworld

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.helloworld.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    
    lateinit var binding: ActivityMainBinding

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        binding.calcular.setOnClickListener {
            var n = binding.horasTrabajadas.text.toString().toInt()
            if ((n<=40)&&(n>=0)) n*=16
            else n = (n-40)*60 + n*16
            binding.nombre.visibility = View.INVISIBLE
            binding.horasTrabajadas.visibility = View.INVISIBLE
            binding.sueldo.text = binding.nombre.text.toString() + ", tu sueldo es de " + n.toString() + "€"
            binding.sueldo.visibility = View.VISIBLE
            binding.calcular.visibility = View.GONE
        }
    }
}