package com.example.conversorunitats

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.conversorunitats.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        binding.converter.setOnClickListener{
            when(binding.userValueSpinner.toString()){
                "Inch" -> when(binding.unitValueSpinner.toString()){
                    "Inch" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*1).toString()
                    "Yard" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*36).toString()
                    "Mile" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*63360).toString()
                    "Centimeter" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*2.54).toString()
                    "Meter" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*39.3700787).toString()
                    else -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*39370.0787).toString()
                }
                "Yard" -> when(binding.unitValueSpinner.toString()){
                    "Inch" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()/36).toString()
                    "Yard" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*1).toString()
                    "Mile" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*1760).toString()
                    "Centimeter" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*91.44).toString()
                    "Meter" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*0.9144).toString()
                    else -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*0.0009144).toString()
                }
                "Mile" -> when(binding.unitValueSpinner.toString()){
                    "Inch" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()/63360).toString()
                    "Yard" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()/1760).toString()
                    "Mile" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*1).toString()
                    "Centimeter" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*160934.4).toString()
                    "Meter" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*1609.344).toString()
                    else -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*1.609344).toString()
                }
                "Centimeter" -> when(binding.unitValueSpinner.toString()){
                    "Inch" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*0.393700787).toString()
                    "Yard" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*0.010936133).toString()
                    "Mile" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*0.00000621).toString()
                    "Centimeter" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*1).toString()
                    "Meter" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*100).toString()
                    else -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*100000).toString()
                }
                "Meter" -> when(binding.unitValueSpinner.toString()){
                    "Inch" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*39.3700787).toString()
                    "Yard" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*1.0936).toString()
                    "Mile" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*0.000621371192).toString()
                    "Centimeter" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()/100).toString()
                    "Meter" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*1).toString()
                    else -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*1000).toString()
                }
                else -> when(binding.unitValueSpinner.toString()){
                    "Inch" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*39370.0787).toString()
                    "Yard" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*1093.6133).toString()
                    "Mile" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*0.621371192).toString()
                    "Centimeter" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()/100000).toString()
                    "Meter" -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()/1000).toString()
                    else -> binding.unitValue.text = (binding.userValue.text.toString().toDouble()*1).toString()
                }
            }
        }
    }
}