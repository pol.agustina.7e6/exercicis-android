package com.example.appamb3pantalles

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import com.example.appamb3pantalles.databinding.ActivitySecondBinding

class SecondActivity : AppCompatActivity() {
    lateinit var binding: ActivitySecondBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivitySecondBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val bundle: Bundle? = intent.extras
        val nom = bundle?.getString("nom")
        binding.seekBar.setOnSeekBarChangeListener(object: OnSeekBarChangeListener{
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                binding.textView.text = p1.toString()
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }

        })
        binding.segonaPagina.setOnClickListener {
            val intent = Intent(this, ThirdActivity::class.java)
            if(binding.hola.isSelected) intent.putExtra("hola_adeu", "Hola")
            else intent.putExtra("hola_adeu", "Adéu")
            intent.putExtra("edat" , binding.textView.text.toString().toInt())
            intent.putExtra("nom", nom)
            startActivity(intent)
        }
    }
}