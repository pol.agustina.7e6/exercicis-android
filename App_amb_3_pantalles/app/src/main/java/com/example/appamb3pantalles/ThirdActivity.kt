package com.example.appamb3pantalles

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.appamb3pantalles.databinding.ActivityThirdBinding

class ThirdActivity : AppCompatActivity() {
    lateinit var binding: ActivityThirdBinding
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityThirdBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val bundle: Bundle? = intent.extras
        val nom = bundle?.getString("nom")
        val salutacio = bundle?.getString("hola_adeu")
        val edat = bundle?.getInt("edat")
        binding.mostra.setOnClickListener {
            binding.mostra.visibility = View.GONE
            binding.missatge.visibility = View.VISIBLE
            if(salutacio == "Hola") binding.missatge.text = "Hola $nom, com portes aquests $edat anys?"
            else binding.missatge.text = "$salutacio $nom, espero tornar a veure’t, abans que facis ${edat?.plus(1)} anys"
        }
    }
}