package com.example.appamb3pantalles

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.appamb3pantalles.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityMainBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        binding.primeraPagina.setOnClickListener {
            val intent = Intent(this, SecondActivity::class.java)
            intent.putExtra("nom", binding.nom.text.toString())
            startActivity(intent)
        }
    }
}