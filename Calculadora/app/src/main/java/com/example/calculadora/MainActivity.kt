package com.example.calculadora

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import com.example.calculadora.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        var num1 = ""
        var num2 = ""
        var missatge = ""
        var operand ='0'
        binding.u.setOnClickListener{
            missatge += "1"
            if (operand == '0') num1+="1"
            else num2+="1"
            binding.sortida.text = missatge
        }
        binding.dos.setOnClickListener{
            missatge += "2"
            if (operand == '0') num1+="2"
            else num2+="2"
            binding.sortida.text = missatge
        }
        binding.tres.setOnClickListener{
            missatge += "3"
            if (operand == '0') num1+="3"
            else num2+="3"
            binding.sortida.text = missatge
        }
        binding.quatre.setOnClickListener{
            missatge += "4"
            if (operand == '0') num1+="4"
            else num2+="4"
            binding.sortida.text = missatge
        }
        binding.cinc.setOnClickListener{
            missatge += "5"
            if (operand == '0') num1+="5"
            else num2+="5"
            binding.sortida.text = missatge
        }
        binding.sis.setOnClickListener{
            missatge += "6"
            if (operand == '0') num1+="6"
            else num2+="6"
            binding.sortida.text = missatge
        }
        binding.set.setOnClickListener{
            missatge += "7"
            if (operand == '0') num1+="7"
            else num2+="7"
            binding.sortida.text = missatge
        }
        binding.vuit.setOnClickListener{
            missatge += "8"
            if (operand == '0') num1+="8"
            else num2+="8"
            binding.sortida.text = missatge
        }
        binding.nou.setOnClickListener{
            missatge += "9"
            if (operand == '0') num1+="9"
            else num2+="9"
            binding.sortida.text = missatge
        }
        binding.zero.setOnClickListener{
            missatge += "0"
            if (operand == '0') num1+="0"
            else num2+="0"
            binding.sortida.text = missatge
        }
        binding.borrar.setOnClickListener{
            missatge = missatge.dropLast(1)
            binding.sortida.text = missatge
        }
        binding.suma.setOnClickListener{
            missatge += " + "
            operand = '+'
            binding.sortida.text = missatge
        }
        binding.resta.setOnClickListener{
            missatge += " - "
            operand = '-'
            binding.sortida.text = missatge
        }
        binding.multiplicacio.setOnClickListener{
            missatge += " x "
            operand = '*'
            binding.sortida.text = missatge
        }
        binding.divisio.setOnClickListener{
            missatge += " / "
            operand = '/'
            binding.sortida.text = missatge
        }
        binding.igual.setOnClickListener{
            when (operand){
                '+' -> missatge="${num1.toInt()+num2.toInt()}"
                '-' -> missatge="${num1.toInt()-num2.toInt()}"
                '*' -> missatge="${num1.toInt()*num2.toInt()}"
                '/' -> missatge="${num1.toInt()/num2.toInt()}"
                else -> missatge="Error de sintaxi"
            }
            binding.sortida.text = missatge
            num1 = ""
            num2 = ""
            missatge = ""
            operand ='0'
        }
    }
}