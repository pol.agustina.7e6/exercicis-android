package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.myapplication.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityMainBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val numerosDados = listOf(R.drawable.dice_1,R.drawable.dice_2,R.drawable.dice_3,R.drawable.dice_4,R.drawable.dice_5,R.drawable.dice_6)
        var numeroDado1 = -1
        var numeroDado2 = -1
        binding.rollerButton.setOnClickListener {
            numeroDado1 = (0..5).random()
            numeroDado2 = (0..5).random()
            if(numeroDado1==5&&numeroDado2==5) Toast.makeText(this, "JACKPOT!", Toast.LENGTH_SHORT).show()
            binding.dado1.setImageResource(numerosDados[numeroDado1])
            binding.dado2.setImageResource(numerosDados[numeroDado2])
        }
        binding.dado1.setOnClickListener {
            numeroDado1 = (0..5).random()
            binding.dado1.setImageResource(numerosDados[numeroDado1])
            if(numeroDado1==5&&numeroDado2==5) Toast.makeText(this, "JACKPOT!", Toast.LENGTH_SHORT).show()
        }
        binding.dado2.setOnClickListener {
            numeroDado2 = (0..5).random()
            binding.dado2.setImageResource(numerosDados[numeroDado2])
            if(numeroDado1==5&&numeroDado2==5) Toast.makeText(this, "JACKPOT!", Toast.LENGTH_SHORT).show()
        }
    }
}