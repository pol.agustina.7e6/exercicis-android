package com.example.endevinaelnumerosecret

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.endevinaelnumerosecret.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        var randomNumber = (1..100).random()
        var tries = 0

        binding.envia.setOnClickListener{
            val userNumber = binding.numero.text.toString().toInt()
            var message = ""
            if(userNumber > randomNumber){
                message = "El numero es mes petit"
            }else if (userNumber < randomNumber){
                message = "El numero es mes gran"
            }else message = "Has encertat"
            tries++
            binding.sortida.text = message + "\nPortes $tries intents"
            binding.sortida.visibility = View.VISIBLE
        }
    }
}