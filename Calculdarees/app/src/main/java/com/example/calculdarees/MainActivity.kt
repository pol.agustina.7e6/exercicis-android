package com.example.calculdarees

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.calculdarees.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.confirma.setOnClickListener{
            when(binding.figures.selectedItem.toString()){
                "Quadrat" -> {
                    binding.missatge.visibility = View.VISIBLE
                    binding.missatge.text = binding.missatge.text.toString() + " d'un quadrat:"
                    binding.editText1.visibility = View.VISIBLE
                    binding.editText1.hint = "Valor dels costats (cm)"
                    binding.editText2.visibility = View.GONE
                }
                "Rectangle" -> {
                    binding.missatge.visibility = View.VISIBLE
                    binding.missatge.text = binding.missatge.text.toString() + " d'un rectangle:"
                    binding.editText1.visibility = View.VISIBLE
                    binding.editText1.hint = "Valor de la base (cm)"
                    binding.editText2.visibility = View.VISIBLE
                    binding.editText2.hint = "Valor de l'altura (cm)"
                }
                "Triangle" -> {
                    binding.missatge.visibility = View.VISIBLE
                    binding.missatge.text = binding.missatge.text.toString() + " d'un triangle:"
                    binding.editText1.visibility = View.VISIBLE
                    binding.editText1.hint = "Valor de la base (cm)"
                    binding.editText2.visibility = View.VISIBLE
                    binding.editText2.hint = "Valor de l'altura (cm)"
                }
                "Rombe" -> {
                    binding.missatge.visibility = View.VISIBLE
                    binding.missatge.text = binding.missatge.text.toString() + " d'un rombe:"
                    binding.editText1.visibility = View.VISIBLE
                    binding.editText1.hint = "Valor de la diagonal major (cm)"
                    binding.editText2.visibility = View.VISIBLE
                    binding.editText2.hint = "Valor de la diagonal menor (cm)"
                }
                "Circumferència" -> {
                    binding.missatge.visibility = View.VISIBLE
                    binding.missatge.text = binding.missatge.text.toString() + " d'una circumferència:"
                    binding.editText1.visibility = View.VISIBLE
                    binding.editText1.hint = "Valor del radi (cm)"
                    binding.editText2.visibility = View.GONE
                }

            }
            binding.calcula.visibility = View.VISIBLE
            binding.confirma.visibility = View.GONE
        }
        binding.calcula.setOnClickListener {
            var area = 0.0
            when(binding.figures.selectedItem.toString()){
                "Quadrat" -> {
                    area = binding.editText1.text.toString().toDouble()*binding.editText1.text.toString().toDouble()
                }
                "Rectangle" -> {
                    area = binding.editText1.text.toString().toDouble()*binding.editText2.text.toString().toDouble()
                }
                "Triangle","Rombe" -> {
                    area = (binding.editText1.text.toString().toDouble()*binding.editText2.text.toString().toDouble())/2.0
                }
                "Circumferència" -> {
                    area = binding.editText1.text.toString().toDouble()*binding.editText1.text.toString().toDouble()*Math.PI
                }
            }
            binding.resultat.text = binding.resultat.text.toString() + " $area cm^2"
            binding.resultat.visibility = View.VISIBLE
            binding.calcula.visibility = View.GONE
        }
    }
}